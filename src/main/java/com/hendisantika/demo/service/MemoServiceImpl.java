package com.hendisantika.demo.service;

import com.hendisantika.demo.entity.Memo;
import com.hendisantika.demo.repository.MemoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/09/18
 * Time: 07.23
 * To change this template use File | Settings | File Templates.
 */
@Service
public class MemoServiceImpl implements MemoService {

    final private MemoRepository memoRepository;

    public MemoServiceImpl(MemoRepository memoRepository) {
        this.memoRepository = memoRepository;
    }

    @Override
    public Page<Memo> findAll(Pageable page) {
        return memoRepository.findAll(page);
    }

}