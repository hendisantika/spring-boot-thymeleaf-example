package com.hendisantika.demo.service;

import com.hendisantika.demo.entity.Memo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/09/18
 * Time: 07.23
 * To change this template use File | Settings | File Templates.
 */
public interface MemoService {
    Page<Memo> findAll(Pageable page);
}