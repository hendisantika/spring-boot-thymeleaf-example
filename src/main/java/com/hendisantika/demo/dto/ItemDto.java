package com.hendisantika.demo.dto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/09/18
 * Time: 07.20
 * To change this template use File | Settings | File Templates.
 */
@Builder
@Value
public class ItemDto implements Serializable {

    private Long id;
    private String name;
    private Integer price;
    private LocalDateTime createAt;

    public static ItemDto of(Long id, String name, Integer price, LocalDateTime createAt) {
        return ItemDto.builder()
                .id(id)
                .name(name)
                .price(price)
                .createAt(createAt)
                .build();
    }
}