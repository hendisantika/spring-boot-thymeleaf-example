package com.hendisantika.demo.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/09/18
 * Time: 07.19
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleForm implements Serializable {

    @NotNull
    @Size(min = 2, max = 120)
    private String singleLineText;

    @NotNull
    @Pattern(regexp = "((19|[2-9][0-9])[0-9]{2})/(0[1-9]|1[0-2])/(0[1-9]|[12][0-9]|3[01])")
    private String textDate;

    @NotNull
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private LocalDate date;

    @NotNull
    @Digits(integer = 9, fraction = 0)
    private String textNum;

    @NotNull
    @Min(0)
    @Max(999999999)
    private Integer num;

    // Optional
    @Size(min = 10, max = 600)
    private String multiLineText;

    // Optional
    @Email
    private String email;

    @NotNull
    @Size(min = 6, max = 99)
    private String password;

    // Optional
    @Pattern(regexp = "A|B|C|D|E")
    private String singleSelect;

    // Optional
    @Size(min = 0, max = 5, message = "{custom.validation.constraints.SelectSize.message}")
    private String[] multiSelects;

    @Pattern(regexp = "on")
    private String singleCheck;

    @NotNull
    @Size(min = 1, max = 5, message = "{custom.validation.constraints.SelectSize.message}")
    private String[] multiChecks;

    @NotNull
    @Pattern(regexp = "A|B|C|D|E")
    private String radio;

    public String getMultiLineTextNl2br() {
        if (this.multiLineText == null || this.multiLineText.length() == 0) {
            return null;
        }
        return this.multiLineText.replaceAll("\n", "<br/>");
    }

}