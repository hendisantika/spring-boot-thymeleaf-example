package com.hendisantika.demo.repository;

import com.hendisantika.demo.entity.Memo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-thymeleaf-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/09/18
 * Time: 07.22
 * To change this template use File | Settings | File Templates.
 */
public interface MemoRepository extends JpaRepository<Memo, Long> {
}